using System;
using Xunit;

using RechercheTableauConsole;

namespace RechercheTableauTests
{
    public class UnitTest1
    {
        int[] integer_tableau = new int[] { 1, 3, 5, 7, 9, 2001, 95, 14, 17, 22 };
        string[] string_tableau = new string[] {"Pierre","Paul","Jacques","Jean","Michel","Ben","Kenobi","Aymeric","Emeric","Arnaud"};
        [Fact]
        public void testIntPresence()
        {
            Assert.True(Program.intPresence(integer_tableau, 2001));
            Assert.False(Program.intPresence(integer_tableau, 20001));
        }
        [Fact]
        public void testIntPosition()
        {
            Assert.Equal(2, Program.intPosition(integer_tableau, 5));
            Assert.Equal(-1, Program.intPosition(integer_tableau, 6969));
        }
        [Fact]
        public void testStrPresence()
        {
            Assert.True(Program.strPresence(string_tableau,"Pierre"));
            Assert.False(Program.strPresence(string_tableau, "Hugo"));
        }
        [Fact]
        public void testStrPosition()
        {
            Assert.Equal(1, Program.strPosition(string_tableau, "Paul"));
            Assert.Equal(-1, Program.strPosition(string_tableau, "Hugo"));
        }
    }
}
