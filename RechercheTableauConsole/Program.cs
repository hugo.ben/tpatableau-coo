﻿using System;

namespace RechercheTableauConsole
{
    public class Program
    {   
        public static bool intPresence(int[] integer_tableau, int searched_number)
        {
            int index;
            for(index=0;index<integer_tableau.Length;index++){
                if(integer_tableau[index] == searched_number){
                    return true;
                }
            }
            return false;
        }
        public static int intPosition(int[] integer_tableau, int searched_number){
            int index;
            for(index=0;index<integer_tableau.Length;index++){
                if(integer_tableau[index] == searched_number){
                    return index;
                }
            }
            return -1;
        }
        public static bool strPresence(string[] string_tableau, string searched_string){
            int index;
            for(index=0;index<string_tableau.Length;index++){
                if(string_tableau[index].Equals(searched_string, StringComparison.InvariantCultureIgnoreCase)){
                    return true;
                }
            }
            return false;
        }
        public static int strPosition(string[] string_tableau, string searched_string){
            int index;
            for(index=0;index<string_tableau.Length;index++){
                if(string_tableau[index].Equals(searched_string, StringComparison.InvariantCultureIgnoreCase)){
                    return index;
                }
            }
            return -1;
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int[] integer_tableau = new int[10];
            string[] string_tableau = new string[10];
        }
    }
}
